package hr.fer.irg.lab1.vjeba2;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

public class Bresenham {
    static{
        GLProfile.initSingleton();
    }

    public static void main(String[] args){
        List<Integer> pointX = new ArrayList<>();
        List<Integer> pointY = new ArrayList<>();

        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run(){

                GLProfile  glprofile = GLProfile.getDefault();
                GLCapabilities glcapabilities = new GLCapabilities(glprofile);
                final GLCanvas glcanvas = new GLCanvas(glcapabilities);

                glcanvas.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        System.out.println("Mis je kliknut na: x=" + e.getX() + " y=" +e.getY());
                        pointX.add(e.getX());
                        pointY.add(e.getY());
                        System.out.println(pointX + "   " + pointY);
                        glcanvas.display();
                        if(pointX.size() >=2 && pointY.size() >=2){
                            pointX.clear();
                            pointY.clear();
                        }
                    }
                });

                glcanvas.addGLEventListener(new GLEventListener() {
                    @Override
                    public void init(GLAutoDrawable glAutoDrawable) {

                    }

                    @Override
                    public void dispose(GLAutoDrawable glAutoDrawable) {

                    }

                    @Override
                    public void display(GLAutoDrawable glAutoDrawable) {
                        GL2 gl2	= glAutoDrawable.getGL().getGL2();  //Returns the GL pipeline object this GLAutoDrawable uses.
                        int width = glAutoDrawable.getSurfaceWidth();
                        int height = glAutoDrawable.getSurfaceHeight();


                        gl2.glClear(GL.GL_COLOR_BUFFER_BIT);  //clear buffers to preset values

                        /*gl2.glLoadIdentity();
                        gl2.glBegin(GL.GL_LINES);
                        gl2.glVertex2f(0, 0);
                        gl2.glVertex2f(width/2, height/2);
                        gl2.glEnd();

                        gl2.glBegin(GL.GL_LINES);
                        gl2.glVertex2f(width/2, height/2);
                        gl2.glVertex2f(width, 0);
                        gl2.glEnd(); */

                        gl2.glLoadIdentity();
                        if(pointX.size() == 2 && pointY.size() == 2){
                            gl2.glBegin(GL.GL_LINES);
                            gl2.glColor3f(1, 1, 1);
                            gl2.glVertex2f(pointX.get(0), height - pointY.get(0) + 100);
                            gl2.glColor3f(1, 0, 0);
                            gl2.glVertex2f(pointX.get(1), height - pointY.get(1) + 100);
                            gl2.glEnd();
                        }

                        if(pointX.size() == 2 && pointY.size() == 2){
                            int xs = pointX.get(0);
                            int ys = height - pointY.get(0);
                            int xe = pointX.get(1);
                            int ye = height - pointY.get(1);
                            gl2.glBegin(GL.GL_POINTS);
                            if(xs <= xe){
                                if(ys <= ye){
                                    bresenham2(gl2, xs, ys, xe, ye);
                                }
                                else{
                                    bresenham3(gl2, xs, ys, xe, ye);
                                }
                            }
                            else{
                                if(ys >= ye){
                                    bresenham2(gl2, xe, ye, xs, ys);
                                }
                                else{
                                    bresenham3(gl2, xe, ye, xs, ys);
                                }
                            }
                            gl2.glEnd();
                        }

                    }

                    @Override
                    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
                        GL2 gl2 = glAutoDrawable.getGL().getGL2(); //Returns the GL pipeline object this GLAutoDrawable uses.
                        gl2.glMatrixMode(GL2.GL_PROJECTION); //Sets the current matrix mode.
                        gl2.glLoadIdentity();  //Load the current matrix with the identity matrix

                        GLU glu = new GLU();  //Provides access to the OpenGL Utility Library (GLU). This library provides standard methods for setting up view volumes, building mipmaps and performing other common operations.
                        glu.gluOrtho2D(0.0f, width, 0.0f, height);

                        gl2.glMatrixMode(GL2.GL_MODELVIEW);  ////Sets the current matrix mode.
                        gl2.glLoadIdentity();  //Load the current matrix with the identity matrix

                        gl2.glViewport(0, 0, width, height); //Entry point to C language function: void glViewport
                    }
                });

                final JFrame jframe = new JFrame("Bresenham ");
                jframe.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
                jframe.addWindowListener(new WindowAdapter() {

                    public void windowClosing(WindowEvent windowevent) {
                        jframe.dispose();
                        System.exit(0);
                    }
                });

                jframe.getContentPane().add(glcanvas, BorderLayout.CENTER);
                jframe.setSize(640, 480);
                jframe.setVisible(true);
                glcanvas.requestFocusInWindow();
            }


        });
    }

    public static void bresenham2(GL2 gl2, int xs, int ys, int xe, int ye){
        int x, yc, korekcija;
        int a, yf;

        if(ye - ys <= xe - xs){
            a = 2*(ye-ys);
            yc = ys;
            yf = -(xe-xs);
            korekcija = -2*(xe-xs);
            for(x = xs; x <= xe; x++){
                gl2.glVertex2f(x, yc);
                yf = yf + a;
                if(yf >= 0){
                    yf = yf + korekcija;
                    yc = yc + 1;
                }
            }
        }
        else{
            x = xe;
            xe = ye;
            ye = x;
            x = xs;
            xs = ys;
            ys = x;
            a = 2 * (ye-ys);
            yc = ys;
            yf = -(xe-xs);
            korekcija = -2*(xe-xs);
            for(x = xs; x <= xe; x++){
                gl2.glVertex2f(yc, x);
                yf = yf + a;
                if(yf >= 0){
                    yf = yf + korekcija;
                    yc = yc + 1;
                }
            }
        }
    }

    public static void bresenham3(GL2 gl2, int xs, int ys, int xe, int ye){
        int x, yc, korekcija;
        int a, yf;

        if(-(ye-ys) <= xe-xs){
            a = 2*(ye-ys);
            yc = ys;
            yf = (xe-xs);
            korekcija = 2*(xe-xs);
            for(x = xs; x <= xe; x++){
                gl2.glVertex2f(x, yc);
                yf = yf + a;
                if(yf <=0){
                    yf = yf + korekcija;
                    yc = yc - 1;
                }
            }
        }
        else{
            x = xe;
            xe = ys;
            ys = x;
            x = xs;
            xs = ye;
            ye = x;
            a = 2*(ye - ys);
            yc = ys;
            yf = (xe - xs);
            korekcija = 2*(xe - xs);
            for(x = xs; x <= xe; x++){
                gl2.glVertex2f(yc, x);
                yf = yf + a;
                if(yf <= 0){
                    yf = yf + korekcija;
                    yc = yc - 1;
                }
            }
        }
    }

}
